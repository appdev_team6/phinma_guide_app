import 'package:flutter/material.dart';
import 'package:phinmaguideapp/about.dart';
import 'package:phinmaguideapp/screens/login.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PHINMA Guide'),
        elevation: 0, // Remove app bar shadow
        backgroundColor: Color(0xFF86A58D), // App bar background color
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("Your Name"),
              accountEmail: Text("your@email.com"),
              currentAccountPicture: CircleAvatar(
                child: Icon(Icons.person),
              ),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xFF86A58D), Color(0xFF618168)],
                ),
              ),
            ),
            ListTile(
              title: Text('Account'),
              leading: Icon(Icons.account_circle, color: Color(0xFF618168)),
              onTap: () {
                Navigator.of(context).pop();
                // Add your account logic here
              },
            ),
            ListTile(
              title: Text('Settings'),
              leading: Icon(Icons.settings, color: Color(0xFF618168)),
              onTap: () {
                Navigator.of(context).pop();
                // Add your settings logic here
              },
            ),
            Divider(),
            ListTile(
              title: Text('Log Out'),
              leading: Icon(Icons.logout, color: Color(0xFF618168)),
              onTap: () {
                Navigator.of(context).pop();
                // Add your logout logic here
              },
            ),
          ],
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xFF86A58D), Color(0xFF618168)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Welcome to PHINMA Guide',
                style: TextStyle(
                    fontSize: 24, color: Colors.white), // Greetings color
              ),
              SizedBox(height: 20),
              Container(
                width: 200,
                height: 50,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  gradient: LinearGradient(
                    colors: [Color(0xFF86A58D), Color(0xFF618168)],
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x40000000),
                      blurRadius: 5,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.transparent,
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25),
                    ),
                  ),
                  onPressed: () {
                    // Add your login logic here
                  },
                  child: Text(
                    'Log In to get started',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Add your search button logic here
        },
        child: Icon(Icons.search, color: Colors.white), // Search icon color
        backgroundColor: Color(0xFF516650), // Search button color
        elevation: 5, // Add shadow to the search button
      ),
    );
  }
}
