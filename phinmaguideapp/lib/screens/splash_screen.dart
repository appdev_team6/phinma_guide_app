import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:phinmaguideapp/screens/home_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);

    Future.delayed(const Duration(seconds: 4), () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (_) => HomePage(),
      ));
    });
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: SystemUiOverlay.values);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFF384D37), // Green
              Color(0xFF618168), // Yellow
            ],
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Replace the Icon widget with Image.asset to display the custom logo
            Image.asset(
              'assets/app_logo.png', // Path to your custom logo
              width: 80, // Set the width and height of your logo
              height: 80,
            ),
            SizedBox(height: 20),
            Text(
              'Phinma Guide',
              style: TextStyle(
                fontStyle: FontStyle.italic,
                color: Colors.white, // Text color
                fontSize: 32,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
