import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFF384D37), // Background color
        body: Container(
          margin: EdgeInsets.all(24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _header(),
              _inputField(context),
              _forgotPassword(),
              _signup(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _header() {
    return Column(
      children: [
        Text(
          "Welcome Back",
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.bold,
            color: Color(0xFF618168), // Text color
          ),
        ),
        Text(
          "Enter your credentials to login",
          style: TextStyle(
            color: Color(0xFF618168), // Text color
          ),
        ),
      ],
    );
  }

  Widget _inputField(context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        TextField(
          decoration: InputDecoration(
            hintText: "Username",
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(18),
              borderSide: BorderSide.none,
            ),
            fillColor: Color(0xFF618168).withOpacity(0.1), // Text field color
            filled: true,
            prefixIcon:
                Icon(Icons.person, color: Color(0xFF618168)), // Icon color
          ),
        ),
        SizedBox(height: 10),
        TextField(
          decoration: InputDecoration(
            hintText: "Password",
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(18),
              borderSide: BorderSide.none,
            ),
            fillColor: Color(0xFF618168).withOpacity(0.1), // Text field color
            filled: true,
            prefixIcon:
                Icon(Icons.lock, color: Color(0xFF618168)), // Icon color
          ),
          obscureText: true,
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            // Add your login logic here
          },
          child: Text(
            "Login",
            style: TextStyle(
              fontSize: 20,
              color: Colors.white, // Text color
            ),
          ),
          style: ElevatedButton.styleFrom(
            primary: Color(0xFF618168), // Button color
            shape: StadiumBorder(),
            padding: EdgeInsets.symmetric(vertical: 16),
          ),
        ),
      ],
    );
  }

  Widget _forgotPassword() {
    return TextButton(
      onPressed: () {
        // Add your forgot password logic here
      },
      child: Text(
        "Forgot password?",
        style: TextStyle(
          color: Color(0xFF618168), // Text color
        ),
      ),
    );
  }

  Widget _signup(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Don't have an account? ",
          style: TextStyle(
            color: Color(0xFF618168), // Text color
          ),
        ),
        TextButton(
          onPressed: () {
            // Add your signup logic here
          },
          child: Text(
            "Sign Up",
            style: TextStyle(
              color: Color(0xFF618168), // Text color
            ),
          ),
        ),
      ],
    );
  }
}
