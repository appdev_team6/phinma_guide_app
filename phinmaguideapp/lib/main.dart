import 'package:flutter/material.dart';
import 'package:phinmaguideapp/screens/splash_screen.dart';
import 'package:phinmaguideapp/screens/home_screen.dart';
import 'package:phinmaguideapp/screens/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PHINMA Guide',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) =>
            SplashScreen(), // Add SplashScreen as your initial screen
        '/home': (context) => HomePage(), // Home screen
        '/login': (context) => LoginPage(), // Login screen
      },
    );
  }
}
